module PitchesSpec where

import Test.Hspec
import Test.Hspec.QuickCheck

import Test.QuickCheck
import Test.QuickCheck.Gen

import HNotes
import HNotes.Pitches

spec_49thKeyPitch :: Expectation
spec_49thKeyPitch = (pitch 49) `shouldBe` 440.0

spec :: Spec
spec = do
  it "49th key has a pitch of 440.0 Hz" $ spec_49thKeyPitch
