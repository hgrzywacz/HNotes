{-|
	Module			: HNotes.SvgRenderer
	Copyright	 : (c) Hubert Grzywacz, 2017
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

{-# LANGUAGE OverloadedStrings #-}

module HNotes.SvgRenderer where

import Control.Monad
import Control.Monad.Trans.Reader
import Control.Exception

import System.Directory
import System.IO.Error

import Data.Text (pack, Text)
import Data.Foldable (asum)

import Text.Blaze.Svg11
import Text.Blaze.Svg.Renderer.Pretty (renderSvg)
import Text.Blaze.Internal (MarkupM)

import qualified Text.Blaze.Svg11.Attributes as A

import HNotes
import HNotes.Utils
import HNotes.Scales
import qualified HNotes.Notes as N

import HNotes.SvgRenderer.Configs
import HNotes.SvgRenderer.Background
import HNotes.SvgRenderer.Translation

noFileHandle :: IOError -> IO ()
noFileHandle e | isDoesNotExistError e = putStrLn "File does not exist"
							 | otherwise = putStrLn "IO Error"

clearFile :: FilePath -> IO ()
clearFile fp = catch (removeFile fp) noFileHandle

noteFill :: ChromaticNote -> Fill
noteFill (ChromaticNote (_, Base)) = Black
noteFill _ = Grey

symbolFill :: Fill -> AttributeValue
symbolFill Black = "#000000"
symbolFill Grey = "#efefef"

textFill :: Fill -> AttributeValue
textFill	Black = "#ffffff"
textFill	Grey = "#00000"

textShift :: Accent -> AttributeValue
textShift Base = "-15"
textShift _ = "-25"

makeEllipse :: AttributeValue -> Svg
makeEllipse sFill =
	ellipse ! A.rx "44" ! A.ry "22" ! A.cx "0" ! A.cy "0" ! A.fill sFill !
		A.stroke "black"

makeText :: AttributeValue -> AttributeValue -> Svg -> Svg
makeText tShift tFill =
	text_ ! A.x tShift ! A.y "14" ! A.fill tFill ! A.fontWeight "bold" !
		A.fontFamily "sans-serif" ! A.fontSize "40px"

noteSymbol :: NoteSpec -> Svg
noteSymbol noteSpec =
	g ! A.transform (makeTranslation fret string) $ do
		makeEllipse sFill
		makeText tShift tFill text
			where
				fret = fretNS noteSpec
				string = stringNS noteSpec
				sFill = symbolFill $ fillS noteSpec
				tFill = textFill $ fillS noteSpec
				tShift = textShift $ accent $ noteS noteSpec
				text = toMarkup $ pack $ show $ noteS noteSpec

type SpecSvg = (NoteSpec, Svg)

svgDoc :: Svg -> SvgConfig
svgDoc objects =
	return $
	docTypeSvg ! A.version "1.1" ! A.width "1000" ! A.height "1000" ! A.viewbox "0 0 1800 1800"
	$ objects

noteSymbolWithSpec :: NoteSpec -> SpecSvg
noteSymbolWithSpec noteSpec = (noteSpec, noteSymbol noteSpec)

fullStringSymbols :: Integer -> (Integer, ChromaticNote, Bool) -> [SpecSvg]
fullStringSymbols fretsN (stringN, open, leftHand) = map noteSymbolWithSpec $
	zipWith zipper notesRange numbersRange
		where
			notesRange = ascend (fromIntegral fretsN + 1) open

			numbersRange = if leftHand then reverse [0..fretsN] else [0..fretsN + 1]

			zipper note fret = NoteSpec {
														noteS = note,
														fillS = noteFill note,
														fretNS = fret,
														stringNS = stringN}

rangeStringSymbols :: Range -> (Integer, ChromaticNote, Bool) -> [SpecSvg]
rangeStringSymbols range (stringN, open, leftHand) = map noteSymbolWithSpec $
	zipWith zipper notesRange numbersRange
		where

			start = fst range
			fretsN = snd range

			end = start + fretsN

			notesRange = takeN fretsN $ dropN start $ ascend (fromIntegral end) open

			numbersRange = if leftHand then reverse [0..end] else [0..(end + 1)]

			zipper note fret = NoteSpec {
														noteS = note,
														fillS = noteFill note,
														fretNS = fret,
														stringNS = stringN}

isLeft :: Hand -> Bool
isLeft RightHand = False
isLeft LeftHand	= True

fullString :: (Note n) => n -> Integer -> Reader FretboardConfig [SpecSvg]
fullString n sn = do
	config <- ask
	let fretsN = fretsNC config
	let hand = handnessC config

	return $ fullStringSymbols fretsN (sn, toChromatic n, isLeft hand)

stringToSvg :: [SpecSvg] -> Svg
stringToSvg = mapM_ snd

runMulti :: Monad m => [Reader r (m a)] -> r -> m ()
runMulti actions r = mapM_ (`runReader` r) actions

strings :: Reader FretboardConfig Svg
strings = do
	config <- ask
	let tunes = tuningC config

	specSvgs <- zipWithM fullString tunes [1..]
	let svgs = concatMap (fmap snd) specSvgs
	let svg = sequence_ svgs

	return svg

filterStringByChord :: Note a => [a] -> [SpecSvg] -> [SpecSvg]
filterStringByChord ch = filter (\specSvg ->
	any (noteEqual (noteS $ fst specSvg)) ch)

type NumberedTuning = [(Integer, ChromaticNote, Bool)]

numberedTuning :: Reader FretboardConfig NumberedTuning
numberedTuning = do
	config <- ask

	let tuning = map toChromatic $ tuningC config
	let range = map toInteger [1..(length tuning)]
	let hand = handnessC config

	return $ zip3 range tuning (repeat $ isLeft hand)

fretboardChord :: Note a => [a] -> SvgConfig
fretboardChord ch = do
	config <- ask
	let fretsN = fretsNC config

	tunes <- numberedTuning

	let filtered =
				map (fullStringSymbols fretsN) tunes &
				map (stringToSvg . filterStringByChord ch) &
				sequence_ &
				return

	return $ runMulti [background, filtered] config

rangeFretboardChord :: Note a => Range -> [a] -> SvgConfig
rangeFretboardChord range ch = do
	tunes <- numberedTuning

	let filtered =
				map (rangeStringSymbols range) tunes &
				map (stringToSvg . filterStringByChord ch) &
				sequence_ &
				return

	config <- ask
	return $ runMulti [rangeBackground range, filtered] config

newtype SvgDoc = SvgDoc (Reader FretboardConfig Svg)

fullFretboard :: SvgDoc
fullFretboard = SvgDoc $ do
	config <- ask

	let actions = [background, strings]
	return $ runMulti actions config

filePath :: FilePath
filePath = "./example.svg"

exampleRange :: Range
exampleRange = (3, 5)

cMajorOnBass :: SvgDoc
cMajorOnBass = SvgDoc $ fretboardChord (majorScale N.c)

gMajorOnBass :: SvgDoc
gMajorOnBass = SvgDoc $ fretboardChord (majorScale N.g)

rangeGMajorOnBass :: SvgDoc
rangeGMajorOnBass = SvgDoc $ rangeFretboardChord exampleRange (majorScale N.g)

render :: SvgDoc -> FilePath -> IO ()
render (SvgDoc svg) filePath = do
	clearFile filePath

	let svgString = renderSvg $ runReader (svg >>= svgDoc) rightHandBass

	writeFile filePath svgString
	putStrLn $ filePath ++ " saved"

example :: IO ()
example = render gMajorOnBass filePath

rangeExample :: IO ()
rangeExample = render rangeGMajorOnBass filePath

fullExample :: IO ()
fullExample = render fullFretboard filePath
