module HNotes.GenericNotes where

import Prelude hiding (read, Read)
import Text.Read (readMaybe)

import HNotes
import HNotes.Scales

-- halfup :: a -> a
-- halfdown :: a -> a

-- |Note without octave.
newtype GenericNote = GenericNote (Key, Accent)

instance Show GenericNote where
	show (GenericNote (k, a)) = show k ++ show a

-- | >>> genericKey (GenericNote (E, Sharp))
-- E
genericKey :: GenericNote -> Key
genericKey (GenericNote (k, _)) = k

-- | >>> genericAccent (GenericNote (E, Sharp))
-- Sharp
genericAccent :: GenericNote -> Accent
genericAccent (GenericNote (_, a)) = a

-- | >>> note2Generic (Note (E, Sharp, 2))
-- GenericNote (E, Sharp)
note2Generic :: Note -> GenericNote
note2Generic (Note (k, a, _)) = GenericNote (k, a)

-- | >>> generic2Note (GenericNote (E, Sharp)) 2
-- Note (E, Sharp, 2)
generic2Note :: GenericNote -> Octave -> Note
generic2Note (GenericNote (k, a)) o = Note (k, a, o)

instance Eq GenericNote where
	(==) gn1 gn2 = generic2Note gn1 0 == generic2Note gn2 0

halfupGeneric :: GenericNote -> GenericNote
halfupGeneric (GenericNote (E, Base)) = GenericNote (F, Base)
halfupGeneric (GenericNote (B, Base)) = GenericNote (C, Base)
halfupGeneric (GenericNote (k, Flat)) = GenericNote (k, Base)
halfupGeneric (GenericNote (k, a)) = GenericNote (nk, na)
	where
		nk = if a == Base then k else next k
		na = next a

halfdownGeneric :: GenericNote -> GenericNote
halfdownGeneric (GenericNote (F, Base)) = GenericNote (E, Base)
halfdownGeneric (GenericNote (C, Base)) = GenericNote (B, Base)
halfdownGeneric (GenericNote (k, Sharp)) = GenericNote (k, Base)
halfdownGeneric (GenericNote (k, a)) = GenericNote (pk, pa)
	where
		pk = if a == Base then k else prev k
		pa = prev a

type GenericStep = GenericNote -> GenericNote

semitonesUpGeneric :: Int -> GenericStep
semitonesUpGeneric n = foldr (.) id (replicate n halfupGeneric)

-- | C note.
firstNote :: GenericNote
firstNote = GenericNote (C, Base)

genericNotetoEnum :: Int -> GenericNote
genericNotetoEnum n = semitonesUpGeneric n firstNote

genericNoteFromEnum :: GenericNote -> Int
genericNoteFromEnum n
	| n == firstNote = 0
	| otherwise = length $ takeWhile (/= firstNote) $ iterate halfdownGeneric n

instance Enum GenericNote where
	toEnum = genericNotetoEnum
	fromEnum = genericNoteFromEnum

instance Looped GenericNote where
	next n = toEnum $ (1 +) $ fromEnum n
	prev n = toEnum $ (1 -) $ fromEnum n

tupleToGenericNote :: (Maybe Key, Maybe Accent) -> Maybe GenericNote
tupleToGenericNote (Just k, Just a) = Just $ GenericNote (k, a)
tupleToGenericNote _ = Nothing

readGenericNote :: String -> Maybe GenericNote
readGenericNote [k] = tupleToGenericNote (read [k], Just Base)
readGenericNote [k, a] = tupleToGenericNote (read [k], read [a])
readGenericNote _ = Nothing

instance Read GenericNote where
	read = readGenericNote

-- | **Unsafe**
genericNote :: String -> GenericNote
genericNote = liftMaybeNote . read
	where
		liftMaybeNote :: Maybe GenericNote -> GenericNote
		liftMaybeNote (Just n) = n
		liftMaybeNote Nothing = error "Cannot read generic note"

type GenericScale = [GenericNote]
type GenericChord = [GenericNote]

genericScale :: GenericNote -> Steps -> GenericScale
genericScale root' steps = map note2Generic $ scale (generic2Note root' 0) steps

genericMajorScale :: GenericNote -> GenericScale
genericMajorScale n = genericScale n majorScaleSteps

genericMinorScale :: GenericNote -> GenericScale
genericMinorScale n = genericScale n majorScaleSteps

genericChromaticScale :: GenericNote -> GenericScale
genericChromaticScale n = take 12 $ iterate halfupGeneric n

type DescribedScale = (String, GenericScale)

pad :: Char -> Int -> String -> String
pad c n s = s ++ replicate (n - length s) c

allScales :: [DescribedScale]
allScales = (++)
	(map (\n -> (pad ' ' 3 (show n) ++ "major", genericMajorScale n)) notes)
	(map (\n -> (pad ' ' 3 (show n) ++ "minor", genericMinorScale n)) notes)
		where
			notes = genericChromaticScale (GenericNote (C, Base))

printDescribedScale :: DescribedScale -> IO ()
printDescribedScale s = do
	putStr $ fst s ++ "	["
	mapM_ (putStr . pad ' ' 3 . show) (snd s)
	putStrLn "]"

printScales :: [DescribedScale] -> IO ()
printScales = mapM_ printDescribedScale

chordInScale :: Chord -> GenericScale -> Bool
chordInScale ch s = all (\n -> note2Generic n `elem` s) ch

findScales :: Chord -> [DescribedScale]
findScales ch = filter (chordInScale ch . snd) allScales

prettyFindScales :: Chord -> IO ()
prettyFindScales ch = do
	printDescribedScale ("Chord\t", map note2Generic ch)
	printScales $ findScales ch
