module ScalesSpec where

import Test.Hspec
import Test.Hspec.QuickCheck

import Test.QuickCheck
import Test.QuickCheck.Gen

import Data.List (intersect)

import HNotes
import HNotes.Scales
import HNotes.Modes

import HNotesSpec

prop_ionianIsMajor :: PlacedNote -> Bool
prop_ionianIsMajor n = (majorScale n) == (makeScale n Ionian)

spec_ionianIsMajor :: Spec
spec_ionianIsMajor =
  prop "Ionian mode scale is the same as major" $ prop_ionianIsMajor

prop_aeolianIsMinor :: PlacedNote -> Bool
prop_aeolianIsMinor n = (minorScale n) == (makeScale n Aeolian)

spec_aeolianIsMinor :: Spec
spec_aeolianIsMinor =
  prop "Aeolian mode scale is the same as minor" $ prop_aeolianIsMinor

spec_modesProps :: SpecWith ()
spec_modesProps = do
  describe "modes properties" $ do
    spec_ionianIsMajor
    spec_aeolianIsMinor

spec :: Spec
spec = do
  spec_modesProps
