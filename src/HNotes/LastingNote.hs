{-# LANGUAGE DeriveLift #-}

{-|
	Module			: HNotes.LastingNote
	Copyright	 : (c) Hubert Grzywacz, 2016
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental

	Notes with time.
-}

module HNotes.LastingNote where

import HNotes
import Data.Map.Lazy as Map
import Data.Char (toLower)
import Data.List (intercalate)
import Language.Haskell.TH.Syntax (Lift)

type Numerator = Int

type Denominator = Int

data Dots = None | Single | Double | Triple deriving (Enum, Eq, Lift)

instance Show Dots where
	show None = ""
	show Single = "."
	show Double = ".."
	show Triple = "..."

newtype NoteValue = NoteValue (Numerator, Denominator, Dots) deriving (Eq, Lift)

instance Show NoteValue where
	show (NoteValue (n, d, dts)) = show n ++ "/" ++ show d ++ show dts

noteValues = Map.fromList [
	("whole", NoteValue (1, 1, None)),
	("half", NoteValue (1, 2, None)),
	("quarter", NoteValue (1, 4, None)),
	("eighth", NoteValue (1, 8, None))
	]

newtype LastingNote = LastingNote (PlacedNote, NoteValue) deriving (Eq, Lift)

instance Show LastingNote where
	show (LastingNote (pn, nv)) = show pn ++ " " ++ show nv

instance HNotes.Read LastingNote where
	read = error "not yet implemented"

instance Note LastingNote where
	key = key . lastingToPlaced
	accent = accent . lastingToPlaced
	halfup (LastingNote (pn, nv)) = LastingNote (halfup pn, nv)
	halfdown (LastingNote (pn, nv)) = LastingNote (halfdown pn, nv)
	note = error "not yet implemented"
	toSharp (LastingNote (pn, nv)) = LastingNote (toSharp pn, nv)
	toFlat (LastingNote (pn, nv)) = LastingNote (toFlat pn, nv)

placedToLasting :: NoteValue -> PlacedNote -> LastingNote
placedToLasting nv pn = LastingNote (pn, nv)

chromaticToLasting :: Octave -> NoteValue -> ChromaticNote -> LastingNote
chromaticToLasting oc nv cn = placedToLasting nv (chromaticToPlaced oc cn)

lastingToPlaced :: LastingNote -> PlacedNote
lastingToPlaced (LastingNote (pn, _)) = pn

wholeValue = noteValues ! "whole"
wholeA = LastingNote (PlacedNote (A, Sharp, 3), noteValues ! "whole")

-- parseKey :: String -> (String, Key)
-- parseKey s = fmap (\k -> ((rest s), k)) (HNotes.read (head s))
