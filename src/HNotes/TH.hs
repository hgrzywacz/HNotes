module HNotes.TH where

import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Data.Char (toLower)

import HNotes
import HNotes.Lilypond

-- keysStrings = map show [A, B, C]

-- TemplateHaskell - generates notes as data consructors
genNotes :: Int -> DecsQ
genNotes n = do
	let typename = mkName "AutoNote"
	let con = [NormalC (mkName $ "Z" ++ show n) []]

	let cxt = []
	let tyVarBndrs = []
	let kind = Nothing

	return [DataD cxt typename tyVarBndrs kind con cxt]

chromaticNoteName :: ChromaticNote -> Name
chromaticNoteName n = mkName $ lilypond (key n) ++ lilypond (accent n)

placedNoteName	:: PlacedNote -> Name
placedNoteName n = mkName $ lilypond (key n) ++ lilypond (accent n) ++ lilypond (octave n)

alternativeName :: (Note a) => a -> [a]
alternativeName n = if accent n == Base then [n] else [n, toFlat n]

generateNotes :: Note a => [a] -> [Name] -> Q [Dec]
generateNotes notes names = do
	liftedNotes <- mapM lift notes
	let clauses = map (\ n -> Clause [] (NormalB n) []) liftedNotes
	return $ zipWith (\name clause -> FunD name [clause]) names clauses

generateAllNotes :: Q [Dec]
generateAllNotes = do
	let startingNote = ChromaticNote (C, Base)
	let allNotes = take 12 $ iterate halfup startingNote
	let allNotesWithAlternatives = allNotes >>= alternativeName
	let names = map chromaticNoteName allNotesWithAlternatives
	generateNotes allNotesWithAlternatives names

generateAllPlacedNotes :: Q [Dec]
generateAllPlacedNotes = do
	let startingNote = PlacedNote (C, Base, 0)
	let allNotes = take (1 + 12 * 9) $ iterate halfup startingNote
	let allNotesWithAlternatives = allNotes >>= alternativeName
	let names = map placedNoteName allNotesWithAlternatives
	generateNotes allNotesWithAlternatives names
