{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleInstances #-}

-- https://wiki.haskell.org/Heterogenous_collections#Existential_types

module HNotes.Lilypond where

import Data.Char (toLower)

import HNotes
import HNotes.LastingNote
import HNotes.Scales

class Lilypond a where
	lilypond :: a -> String
	p :: a -> Lilypondable
	p = pack

instance Lilypond Accent where
	lilypond Sharp = "sharp"
	lilypond Flat = "flat"
	lilypond Base = ""

data Lilypondable = forall a . Lilypond a => MkLilypondable a

pack :: Lilypond a => a -> Lilypondable
pack = MkLilypondable

instance Lilypond Lilypondable where
	lilypond (MkLilypondable n) = lilypond n

instance Lilypond Key where
	lilypond = Prelude.map toLower . show

instance Lilypond Octave where
	lilypond = show

instance Lilypond NoteValue where
	lilypond (NoteValue (n, d, ds)) = show d

instance Lilypond ChromaticNote where
	lilypond (ChromaticNote (k, a)) = lilypond k ++ lilypond a

instance Lilypond PlacedNote where
	lilypond pn = lilypond (key pn) ++ lilypond (accent pn)

instance Lilypond LastingNote where
	lilypond ln = lilypond (key ln) ++ lilypond (accent ln)

instance (Lilypond n) => Lilypond [n] where
	lilypond = unwords . map lilypond

newtype Score = Score [Lilypondable]

unscore :: Score -> [Lilypondable]
unscore (Score s) = s

instance Lilypond Score where
	lilypond (Score score) = unwords $ fmap lilypond score

score :: Score
score = Score [p (ChromaticNote (C, Sharp)), p (ChromaticNote (A, Base))]
