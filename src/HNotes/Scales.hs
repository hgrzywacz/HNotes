{-|
	Module			: HNotes.Scales
	Copyright	 : (c) Hubert Grzywacz, 2016
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental

	Collection of function and constants to generate basic musical scales.
-}

module HNotes.Scales
	-- (
	-- chromaticScaleAscend,
	-- chromaticScaleDescent,
	-- chromaticScale,
	-- majorScale,
	-- -- majorScale',
	-- majorScaleSteps,
	-- minorScale,
	-- minorScaleSteps
-- )
where

import HNotes

-- * Scale datatype

-- * Chromatic Scales

-- |Generate an ascending chromatic scale starting from a note.
--
-- >>> printScaleLn $ chromaticScaleAscend $ placedNote "C2"
-- [C2 C#2 D2 D#2 E2 F2 F#2 G2 G#2 A2 A#2 B2]
chromaticScaleAscend ::	(Note a) => a -> [a]
chromaticScaleAscend = ascend 12

-- |Generate a descending chromatic scale starting from a note.
--
-- >>> printScaleLn $ chromaticScaleDescent $ note "A4"
-- [A4 A♭4G4 G♭4F4 E4 E♭4D4 D♭4C4 B3 B♭3]
chromaticScaleDescent :: (Note a) => a -> [a]
chromaticScaleDescent = descent 12

-- |Alias for 'chromaticScaleAscend'.
chromaticScale :: (Note a) => a -> [a]
chromaticScale = chromaticScaleAscend

tetrachordSteps :: (Note a) => [a -> a]
tetrachordSteps = [oneup, oneup, halfup]

-- * Diatonic scales

-- |Steps for major scale:
-- whole, whole, half, whole, whole, whole, half
majorScaleSteps :: (Note a) => [a -> a]
majorScaleSteps = tetrachordSteps ++ [oneup] ++ tetrachordSteps

-- |Steps for minor scale:
-- whole, half, whole, whole, half, whole, whole
minorScaleSteps :: (Note a) => [a -> a]
minorScaleSteps = [w, h, w, w, h, w, w]

-- |Generate a major diatonic scale based on given root following progression:
--
-- > whole, whole, half, whole, whole, whole, half
majorScale :: (Note a) => a -> [a]
majorScale root' = scale root' majorScaleSteps

-- |Generate a major diatonic scale based on given root following progression:
--
-- > whole, half, whole, whole, half, whole, whole
minorScale :: (Note a) => a -> [a]
minorScale root' = scale root' minorScaleSteps

-- |Steps for minor pentatonic scale
--
-- > whole and a half, whole, whole, whole and a half, whole
pentatonicMinorScaleSteps :: (Note a) => [a -> a]
pentatonicMinorScaleSteps = [w .h, w, w, w . h, w]

-- |Generate pentatonic minor scale
pentatonicMinorScale :: (Note a) => a -> [a]
pentatonicMinorScale root' = scale root' pentatonicMinorScaleSteps

-- |Steps fo blues scale
--
-- Adds diminished fifth <http://12bar.de/cms/tutorial/scales/ blue note>
bluesScaleSteps :: (Note a) => [a -> a]
bluesScaleSteps = [w . h, w, h, h, w . h, w]

-- |Generate blues scale with dimished fifth
bluesScale :: (Note a) => a -> [a]
bluesScale root' = scale root'	bluesScaleSteps

type ChromaticScale = [ChromaticNote]

type DescribedScale = (String, ChromaticScale)

pad :: Char -> Int -> String -> String
pad c n s = s ++ replicate (n - length s) c

describeScales :: (Note n) => String -> (n -> ChromaticScale) -> n -> DescribedScale
describeScales name scale note = (sName, scale note)
	where
		sName = pad ' ' 3 (show note) ++ name

allScales :: [DescribedScale]
allScales =	concat [
		map (describeScales "major" majorScale) notes,
		map (describeScales "minor" minorScale) notes,
		map (describeScales "pentatonic" pentatonicMinorScale) notes,
		map (describeScales "blues" bluesScale) notes]
		where
			notes = chromaticScale (ChromaticNote (C, Base))

printDescribedScale :: DescribedScale -> IO ()
printDescribedScale s = do
	putStr $ fst s ++ "  ["
	mapM_ (putStr . pad ' ' 3 . show) (snd s)
	putStrLn "]"

printScales :: [DescribedScale] -> IO ()
printScales = mapM_ printDescribedScale

containsNote :: Note a => ChromaticScale -> a -> Bool
containsNote s n = any (noteEqual n) s

chordInScale :: Note a => [a] -> ChromaticScale -> Bool
chordInScale ch s = all (containsNote s) ch

findScales :: Note a => [a] -> [DescribedScale]
findScales ch = filter (chordInScale ch . snd) allScales
