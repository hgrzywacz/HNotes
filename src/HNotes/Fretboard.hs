module HNotes.Fretboard where

-- import System.Console.ANSI
import Data.Function ((&))
import Data.List (intersperse, find)

import HNotes
import HNotes.Tunings
import HNotes.Coloring

data Separator = Fret | Space | Tab | None

data DotsPattern = DotsPattern {singleDot :: String,
																doubleDot :: String,
																emptySpace :: String,
																singles :: [Int],
																doubles :: [Int],
																number :: Int}

separate :: Separator -> ([IO ()] -> [IO ()])
separate Fret = intersperse $ putStr " | "
separate Space = intersperse $ putStr " "
separate Tab = intersperse $ putStr "\t"
separate None = id

intersperseSeparator :: Separator -> [String] -> [String]
intersperseSeparator Fret = intersperse " | "
intersperseSeparator Space = intersperse " "
intersperseSeparator Tab = intersperse "\t"
intersperseSeparator None = id

printString :: Int -> Separator -> PlacedNote -> IO ()
printString nFrets separator open = sequence_ $ separate separator $ map printPlacedNote $ ascend nFrets open

printStringFlats :: Int -> Separator -> PlacedNote -> IO ()
printStringFlats nFrets separator open =
	sequence_ $ separate separator $ map (printPlacedNote . toFlat) (ascend nFrets open)

-- show number in 4 spaces
show4 :: (Show a, Ord a, Num a) => a -> String
show4 n | n < 10 = show n ++ "  "
				| otherwise = show n ++ " "

printFretNumbers :: Int -> Separator -> IO ()
printFretNumbers nFrets separator =
	sequence_ (separate separator $ map (putStr . show4)	$ take nFrets nums)	>> putStrLn ""
		where
			nums = [0..] :: [Int]

-- |
-- <https://en.wikipedia.org/wiki/Augmented_fifth Ibanez GSR200B>
ibanezDotsPattern :: DotsPattern
ibanezDotsPattern = DotsPattern {singleDot = "•  ",
																 doubleDot = "•• ",
																 emptySpace = "  ",
																 singles = [3,5,7,9],
																 doubles = [12],
																 number = 12}

-- |Returns `nFrets` long row of dots as configured in dotsPattern
dotStrings :: Int -> Separator -> DotsPattern -> [String]
dotStrings nFrets separator dotsPattern =
	insertSingles emptyFretboard &
	insertDoubles &
	take (number dotsPattern) &
	cycle &
	take (nFrets - 1) &
	(\list -> emptySpace dotsPattern : list) &
	intersperseSeparator separator
		where
			emptyFretboard = repeat $ emptySpace dotsPattern

			insertDots :: [Int] -> String -> [String] -> [String]
			insertDots indices dot list =
				foldl (\memo i -> let (xs, ys) = splitAt i memo in init xs ++ [dot] ++ ys) list indices

			insertSingles :: [String] -> [String]
			insertSingles = insertDots (singles dotsPattern) (singleDot dotsPattern)

			insertDoubles :: [String] -> [String]
			insertDoubles = insertDots (doubles dotsPattern) (doubleDot dotsPattern)

printDots :: Int -> Separator -> DotsPattern -> IO ()
printDots nFrets separator dotsPattern =
	mapM_ putStr (dotStrings nFrets separator dotsPattern) >> putStrLn ""

printFretboard :: Int -> Separator -> Tuning	-> IO ()
printFretboard nFrets separator tuning =
	sequence_ (intersperse (putStrLn "") $
		map (printString nFrets separator) (reverse tuning)) >> putStrLn ""

data Accents = Sharps | Flats

accents (Just Flat) = Flats
accents _					 = Sharps

mapAccents :: (Functor t, Note a) => Accents -> t a -> t a
mapAccents Sharps = fmap toSharp
mapAccents Flats = fmap toFlat

-- | Takes a predicate indicating which notes to display as a first argument.
printFilteredString :: (PlacedNote -> Bool) -> Accents -> Int -> Separator -> PlacedNote -> IO ()
printFilteredString p accents nFrets separator open =
	sequence_ $ separate separator $ map (printFilteredNote p) $ mapAccents accents $ ascend nFrets open

printFilteredFretboard :: (PlacedNote -> Bool) -> Accents -> Int -> Separator -> Tuning -> IO ()
printFilteredFretboard p accents nFrets separator tuning =
	sequence_
		(intersperse (putStrLn "") $
			map (printFilteredString p accents nFrets separator) (reverse tuning)) >> putStrLn ""

printFretboardFlats :: Int -> Separator -> Tuning	-> IO ()
printFretboardFlats nFrets separator tuning =
	sequence_
		(intersperse (putStrLn "") $
			map (printStringFlats nFrets separator) (reverse tuning)) >> putStrLn ""

printStandardFretboardFiltered :: Tuning -> Accents -> (PlacedNote -> Bool) -> IO ()
printStandardFretboardFiltered tuning accents p = do
	let nFrets = 17
	let separator = Fret
	let dotsPattern = ibanezDotsPattern
	printFretNumbers nFrets separator
	printDots nFrets separator dotsPattern
	printFilteredFretboard p accents nFrets separator tuning

printStandardBassFretboardFiltered :: Accents -> (PlacedNote -> Bool) -> IO ()
printStandardBassFretboardFiltered = printStandardFretboardFiltered eStandard

printStandardGuitarFretboardFiltered :: Accents -> (PlacedNote -> Bool) -> IO ()
printStandardGuitarFretboardFiltered = printStandardFretboardFiltered eGuitarStandard

printStandardBassFretboard :: IO ()
printStandardBassFretboard = printStandardBassFretboardFiltered Sharps (const True)

printStandardGuitarFretboard :: IO ()
printStandardGuitarFretboard = printStandardGuitarFretboardFiltered Sharps (const True)

firstAccent :: (Note a) => [a] -> Maybe Accent
firstAccent = find (Base /=) . map accent

printChordOnFretboard :: (Note a) => [a] -> IO ()
printChordOnFretboard ch =
	printStandardBassFretboardFiltered (accents first) (\n -> any (noteEqual n) ch)
		where
			first = firstAccent ch

printChordOnGuitarFretboard :: (Note a) => [a] -> IO ()
printChordOnGuitarFretboard ch =
	printStandardGuitarFretboardFiltered (accents first) (\n -> any (noteEqual n) ch)
		where
			first = firstAccent ch

printChordsOnFretboard :: (Note a) => [[a]] -> IO ()
printChordsOnFretboard chs = sequence_ $ intersperse (putStr "\n\n") $ map printChordOnFretboard chs

-- printFlatsStandardFretboard :: IO ()
-- printFlatsStandardFretboard = do
--	 let nFrets = 17
--	 let separator = Fret
--	 let tuning = eStandard
--	 let dotsPattern = ibanezDotsPattern
--	 printFretNumbers nFrets separator
--	 printDots nFrets separator dotsPattern
--	 printFretboardFlats nFrets separator tuning
