{-|
	Module			: HNotes
	Description : Musical suite.
	Copyright	 : (c) Hubert Grzywacz, 2016
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE DeriveLift #-}

module HNotes where

import Prelude hiding (read, Read)
import Text.Read (readMaybe)
import Language.Haskell.TH.Syntax

-- | Data that loops around.
class Enum a => Looped a where
	next :: a -> a
	prev :: a -> a

class Read a where
	read :: String -> Maybe a

liftMaybe :: Maybe a -> a
liftMaybe (Just n) = n
liftMaybe Nothing = error "Cannot read generic note"

-- | Basic tone of a note.
data Key = C | D | E | F | G | A | B deriving (Eq, Ord, Show, Enum, Lift)

instance Looped Key where
	next B = C
	next n = succ n
	prev C = B
	prev n = pred n

instance Read Key where
	read "C" = Just C
	read "D" = Just D
	read "E" = Just E
	read "F" = Just F
	read "G" = Just G
	read "A" = Just A
	read "B" = Just B
	read _ = Nothing

-- | Accidental of a note.
data Accent = Flat | Base | Sharp deriving (Eq, Ord, Enum, Lift)

-- defaults to sharps
instance Looped Accent where
	next Sharp = Base
	next Flat = Base
	next Base = Sharp

	prev Sharp = Base
	prev Flat = Base
	prev Base = Flat

instance Read Accent where
	read "b" = Just Flat
	read "♭" = Just Flat
	read "" = Just Base
	read " " = Just Base
	read "#" = Just Sharp
	read _ = Nothing

showAccent :: Accent -> String
showAccent Flat = "b" --"♭"
showAccent Base = ""
showAccent Sharp = "#"

showFancyAccent :: Accent -> String
showFancyAccent Flat = "♭"
showFancyAccent Base = ""
showFancyAccent Sharp = "♯"

instance Show Accent where
	show = showFancyAccent

-- * Note

-- | Typeclass for musical notes.
class (Show a, Eq a, Read a, Lift a) => Note a where
	key :: a -> Key
	accent :: a -> Accent
	halfup :: a -> a
	halfdown :: a -> a
	note :: String -> a
	toSharp :: a -> a
	toFlat :: a -> a

	scale :: a -> [a -> a] -> [a]
	scale = scanl (\memo step -> (step memo))

	-- scale' :: a -> [(a -> a)] -> Scale a
	-- scale' r steps = Scale $ scanl (\memo step -> (step memo)) r steps

-- data Scale n = Scale [n] deriving (Show, Eq)

-- instance Functor Scale where
--	 fmap f (Scale ns) = Scale $ map f ns

-- instance Foldable Scale where
--	 foldr f n (Scale ns) = foldr f n ns

-- type Step = forall a . (Note a) => (a -> a)
-- type Interval = (Note a) => (a -> a)

-- type Steps = [Step]
-- type Intervals = [Interval]

-- type Scale = forall a . (Note a) => [a]
-- type Chord = forall a . (Note a) => [a]

-- ** Chromatic Note

-- | Simplest note with `Key` nad `Accent` only.
newtype ChromaticNote = ChromaticNote (Key, Accent) deriving (Lift)

instance Read ChromaticNote where
	read = readChromaticNote

instance Show ChromaticNote where
	show (ChromaticNote (k, a)) = show k ++ show a

instance Eq ChromaticNote where
	(==) (ChromaticNote (nk, Sharp)) (ChromaticNote (mk, Flat)) = mk == next nk
	(==) (ChromaticNote (nk, Flat)) (ChromaticNote (mk, Sharp)) = mk == prev nk
	(==) (ChromaticNote (nk, na)) (ChromaticNote (mk, ma)) = nk == mk && na == ma

instance Note ChromaticNote where
	key (ChromaticNote (k, _)) = k
	accent (ChromaticNote (_, a)) = a
	halfup = halfupChromatic
	halfdown = halfdownChromatic
	note = chromaticNote

	toSharp (ChromaticNote (k, Flat)) = ChromaticNote (prev k, Sharp)
	toSharp n = n

	toFlat (ChromaticNote (k, Sharp)) = ChromaticNote (next k, Flat)
	toFlat n = n

instance Enum ChromaticNote where
	fromEnum cn = length $ takeWhile (/= cn) fullScale
		where
			fullScale :: [ChromaticNote]
			fullScale = take 12 $ iterate halfup $ ChromaticNote (C, Base)
	toEnum n = fullScale !! n
		where
			fullScale :: [ChromaticNote]
			fullScale = iterate halfup $ ChromaticNote (C, Base)

tupleToChromaticNote :: (Maybe Key, Maybe Accent) -> Maybe ChromaticNote
tupleToChromaticNote (Just k, Just a) = Just $ ChromaticNote (k, a)
tupleToChromaticNote _ = Nothing

-- | Read ChromaticNote from String.
--
-- >>> readChromaticNote "A#"
-- Just A#
--
-- >>> readChromaticNote "Z"
-- Nothing
readChromaticNote :: String -> Maybe ChromaticNote
readChromaticNote [k] = tupleToChromaticNote (read [k], Just Base)
readChromaticNote [k, a] = tupleToChromaticNote (read [k], read [a])
readChromaticNote _ = Nothing

-- | __Unsafe__
--
-- Same as 'readChromaticNote' but omits 'Maybe', raises an exception istead of returning 'Nothing'.
--
-- >>> chromaticNote "AZ"
-- *** Exception: Cannot read generic note
chromaticNote :: String -> ChromaticNote
chromaticNote = liftMaybe . readChromaticNote

-- | Raise 'ChromaticNote' by a semitone.
--
-- >>> halfupChromatic $ chromaticNote "A#"
-- B
halfupChromatic :: ChromaticNote -> ChromaticNote
halfupChromatic (ChromaticNote (E, Base)) = ChromaticNote (F, Base)
halfupChromatic (ChromaticNote (B, Base)) = ChromaticNote (C, Base)
halfupChromatic (ChromaticNote (k, Flat)) = ChromaticNote (k, Base)
halfupChromatic (ChromaticNote (k, a)) = ChromaticNote (nk, na)
	where
		nk = if a == Base then k else next k
		na = next a

-- | Raise 'ChromaticNote' by a semitone.
--
-- >>> halfdownChromatic $ chromaticNote "A#"
-- A
halfdownChromatic :: ChromaticNote -> ChromaticNote
halfdownChromatic (ChromaticNote (F, Base)) = ChromaticNote (E, Base)
halfdownChromatic (ChromaticNote (C, Base)) = ChromaticNote (B, Base)
halfdownChromatic (ChromaticNote (k, Sharp)) = ChromaticNote (k, Base)
halfdownChromatic (ChromaticNote (k, a)) = ChromaticNote (pk, pa)
	where
		pk = if a == Base then k else prev k
		pa = prev a

-- ** PlacedNote

-- | Note with octave.
newtype PlacedNote = PlacedNote (Key, Accent, Octave) deriving (Lift)

type Octave = Int

instance Read Octave where
	read = readMaybe

instance Read PlacedNote where
	read = readPlacedNote

instance Show PlacedNote where
	show (PlacedNote (k, a, o)) = show k ++ show a ++ show o

instance Eq PlacedNote where
	(==) (PlacedNote (nk, Sharp, no)) (PlacedNote (mk, Flat, mo)) = mk == next nk && no == mo
	(==) (PlacedNote (nk, Flat, no)) (PlacedNote (mk, Sharp, mo)) = mk == prev nk && no == mo
	(==) (PlacedNote (nk, na, no)) (PlacedNote (mk, ma, mo)) = nk == mk && na == ma && no == mo

instance Ord PlacedNote where
	compare a b | a == b = EQ
							| octave a < octave b = LT
							| key a < key b = LT
							| accent a < accent b = LT
							| otherwise = GT

instance Enum PlacedNote where
	-- fromEnum (PlacedNote (k, a, o)) = ((fromEnum k)) + ((fromEnum a) - 1) + (o * 12) + 1
	fromEnum pn = fromEnum (toChromatic pn) + (octave pn * 12)
	toEnum n = chromaticToPlaced (div n 12) (toEnum n)

distance :: PlacedNote -> PlacedNote -> Int
distance fromNote toNote = fromEnum fromNote - fromEnum toNote

instance Note PlacedNote where
	key (PlacedNote (k, _, _)) = k
	accent (PlacedNote (_, a, _)) = a
	halfup = halfupPlaced
	halfdown = halfdownPlaced
	note = placedNote

	toSharp (PlacedNote (k, Flat, o)) = PlacedNote (prev k, Sharp, o)
	toSharp n = n

	toFlat (PlacedNote (k, Sharp, o)) = PlacedNote (next k, Flat, o)
	toFlat n = n

-- | Extract `Octave` from `PlacedNote`.
octave :: PlacedNote -> Octave
octave (PlacedNote (_, _, o)) = o

tupleToPlacedNote :: (Maybe Key, Maybe Accent, Maybe Octave) -> Maybe PlacedNote
tupleToPlacedNote (Just k, Just a, Just o) = Just $ PlacedNote (k, a, o)
tupleToPlacedNote _ = Nothing

-- | Read ChromaticNote from String.
--
-- >>> readPlacedNote "A#3"
-- Just A#3
--
-- >>> readPlacedNote "Z4"
-- Nothing
readPlacedNote :: String -> Maybe PlacedNote
readPlacedNote [k, o] = tupleToPlacedNote	(read [k], Just Base, read [o])
readPlacedNote [k, a, o] = tupleToPlacedNote	(read [k], read [a], read [o])
readPlacedNote _ = Nothing

-- | __Unsafe__
--
-- Same as readPlacedNote but omits 'Maybe', raises an exception istead of returning 'Nothing'.
--
-- >>> placedNote "Z4"
-- *** Exception: Cannot read generic note
placedNote :: String -> PlacedNote
placedNote = liftMaybe . readPlacedNote

-- | Convert 'PlacedNote' to 'ChromaticNote' losing information about octave.
--
-- >>> placedToChromatic $ placedNote "A#3"
-- A#
placedToChromatic :: PlacedNote -> ChromaticNote
placedToChromatic (PlacedNote (k, a, _)) = ChromaticNote (k, a)

-- | Convert 'ChromaticNote' to 'PlacedNote' adding octave.
--
-- >>> chromaticToPlaced 4 (ChromaticNote (A, Sharp))
-- A#4
chromaticToPlaced :: Octave -> ChromaticNote -> PlacedNote
chromaticToPlaced o (ChromaticNote (k, a)) = PlacedNote (k, a, o)


halfupPlaced :: PlacedNote -> PlacedNote
halfupPlaced (PlacedNote (E, Base, o)) = PlacedNote (F, Base, o)
halfupPlaced (PlacedNote (B, Base, o)) = PlacedNote (C, Base, o + 1)
halfupPlaced (PlacedNote (k, Flat, o)) = PlacedNote (k, Base, o)
halfupPlaced (PlacedNote (k, a, o)) = PlacedNote (nk, na, o)
	where
		nk = if a == Base then k else next k
		na = next a

halfdownPlaced :: PlacedNote -> PlacedNote
halfdownPlaced (PlacedNote (F, Base, o)) = PlacedNote (E, Base, o)
halfdownPlaced (PlacedNote (C, Base, o)) = PlacedNote (B, Base, o - 1)
halfdownPlaced (PlacedNote (k, Sharp, o)) = PlacedNote (k, Base, o)
halfdownPlaced (PlacedNote (k, a, o)) = PlacedNote (pk, pa, o)
	where
		pk = if a == Base then k else prev k
		pa = prev a

-- * Functions on notes
-- | Converts flats to sharp, sharp to flats and bases to bases.
swapAccents :: (Note a) => a -> a
swapAccents n
	| accent n == Flat = toSharp n
	| accent n ==	Sharp = toFlat n
	| otherwise = n

-- * Intervals

-- | Semitone step.
h :: (Note a) => a -> a
h = halfup

-- | Whole tone step.
w :: (Note a) => a -> a
w = halfup . halfup

-- | Same note.
root :: a -> a
root = id

-- | Whole tone up
oneup :: (Note a) => a -> a
oneup = w

-- | Whole tone down.
onedown :: (Note a) => a -> a
onedown = halfdown . halfdown

-- | Octave up
octaveUp :: (Note a) => a -> a
octaveUp = (!! 12) . iterate halfup

-- * Functions on notes

-- | Turns any `Note` into `ChromaticNote`, loosing all info except `Key` and
-- `Accent`.
--
-- >>> toChromatic $ placedNote "E#3"
-- E#
toChromatic :: (Note a) => a -> ChromaticNote
toChromatic n = ChromaticNote (key n, accent n)

-- | Equality of notes:
--
-- >>> noteEqual (PlacedNote (C, Sharp, 2)) (ChromaticNote (C, Sharp))
-- True
--
-- >>> noteEqual (PlacedNote (C, Sharp, 2)) (ChromaticNote (D, Flat))
-- True
--
-- >>> noteEqual (PlacedNote (C, Sharp, 2)) (ChromaticNote (D, Base))
-- False
noteEqual :: (Note a, Note b) => a -> b -> Bool
noteEqual n1 n2 = toChromatic n1 == toChromatic n2

ascend :: (Note a) => Int -> a -> [a]
ascend subtones start = take subtones $ iterate halfup start

descent :: (Note a) => Int -> a -> [a]
descent subtones start = take subtones $ iterate halfdown start

-- | Returns a note @n@ semitones up from a given note.
--
-- >>> semitonesUp 4 (ChromaticNote (C, Base))
-- E
--
-- >>> semitonesUp 4 (PlacedNote (B, Base, 4))
-- D#5
semitonesUp :: (Note a) => Int -> a -> a
semitonesUp n = foldr (.) id (replicate n halfup)

-- | Returns a note @n@ semitones up from a given note.
--
-- >>> semitonesDown 4 (ChromaticNote (C, Base))
-- A♭
--
-- >>> semitonesDown 4 (PlacedNote (B, Base, 4))
-- G4
semitonesDown :: (Note a) => Int -> a -> a
semitonesDown n = foldr (.) id (replicate n halfdown)

-- | Generate chord from a given tonic and list of intervals.
--
-- / Example: major triad/
--
-- >>> chord (ChromaticNote A, Base) [root, (semitonesUp 4), (semitonesUp 7)]
-- [A,C#,E]
--
-- / Example: minor triad - placed notes/
--
-- >>> chord (ChromaticNote A, Base) [root, (semitonesUp 3), (semitonesUp 7)]
-- [A4,C5,E5]
chord :: a -> [a -> a] -> [a]
chord r = map (\interval -> interval r)
