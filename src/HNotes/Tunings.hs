module HNotes.Tunings (Tuning, eStandard, eGuitarStandard) where

import HNotes

type Tuning = [PlacedNote]

e1 :: PlacedNote
e1 = PlacedNote (E, Base, 1)

a1 :: PlacedNote
a1 = PlacedNote (A, Base, 1)

d2 :: PlacedNote
d2 = PlacedNote (D, Base, 2)

g2 :: PlacedNote
g2 = PlacedNote (G, Base, 2)


e2 :: PlacedNote
e2 = PlacedNote (E, Base, 2)

a2 :: PlacedNote
a2 = PlacedNote (A, Base, 2)

d3 :: PlacedNote
d3 = PlacedNote (D, Base, 3)

g3 :: PlacedNote
g3 = PlacedNote (G, Base, 3)

b3 :: PlacedNote
b3 = PlacedNote (B, Base, 3)

e4 :: PlacedNote
e4 = PlacedNote (E, Base, 4)

-- |Standard bass guitar tuning of E1-A1-D1-G1.
eStandard :: Tuning
eStandard = [e1, a1, d2, g2]

-- |Standard electric guitar tuning E2-A2-D3-G3-B3-E4.
eGuitarStandard :: Tuning
eGuitarStandard = [e2, a2, d3, g3, b3, e4]
