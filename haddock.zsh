#! /usr/bin/env zsh

me=`basename "$0"`
command=$@[1]

printHelp() {
  echo "Usage: \$ $me COMMAND"
  echo "Available commands:"
  echo "build - generate docs"
  echo "open - build and open docs in browser"
  echo "watch - build and watch files for changes"
}

generateDocs() {
  echo "Building docs";
  command stack haddock;
}

generateAndOpen() {
  generateDocs
  command stack haddock --open HNotes
}

watchFiles() {
  generateAndOpen
  command stack haddock --file-watch HNotes
}


if [[ $command = build ]]; then
  generateDocs
elif [[ $command = open ]]; then
  generateAndOpen
elif [[ $command = watch ]]; then
  watchFiles
elif [[ $command = -h ]]; then
  printHelp
else
  generateDocs
fi
