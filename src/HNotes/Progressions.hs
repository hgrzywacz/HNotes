{-|
	Module			: HNotes.Progressions
	Copyright	 : (c) Hubert Grzywacz, 2016
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental

	Chords progressions.
-}

module HNotes.Progressions where

import HNotes
import HNotes.Scales
import HNotes.Chords

-- | Levels pattern:
--
-- > I		 |ii		|iii	 |IV		|V		 |vi		|vii
-- > Major |Minor |Minor |Major |Major |Minor |Diminished
--
-- >>> triadByNumber "ii" $ c2
-- [C2,D#2,G2]
{-# ANN triadByNumber "HLint: ignore" #-}
triadByNumber :: (Note a) => String -> [a] -> [a]
triadByNumber "I" s = majorTriad $ s !! 0
triadByNumber "ii" s = minorTriad $ s !! 1
triadByNumber "iii" s = minorTriad $ s !! 2
triadByNumber "IV" s = majorTriad $ s !! 3
triadByNumber "V" s = majorTriad $ s !! 4
triadByNumber "vi" s = minorTriad $ s !! 5
triadByNumber "vii" s = diminishedTriad $ s !! 6
triadByNumber num _ = error ("Chord " ++ num ++ " does not exist")

-- |
-- >>> makeTriadProgression ["I", "V"] $ majorScale $ note "E2"
--[[E2,G#2,B2],[B2,D#3,F#3]]
makeTriadProgression :: (Note a) => [String] -> [a] -> [[a]]
makeTriadProgression levels s = map (flip triadByNumber $ s) levels

-- |
-- > I - V - vi - IV
--
-- https://www.taylorguitars.com/blog/lessons-tips/what-most-common-chord-progression-world-shawn-persinger
fourChordSongProgression :: [String]
fourChordSongProgression = ["I", "V", "vi", "IV"]

-- |
-- <https://www.youtube.com/watch?v=lwlogyj7nFE Red Hot Chili Peppers - Under The Bridge>
--
-- https://tabs.ultimate-guitar.com/r/red_hot_chili_peppers/under_the_bridge_crd.htm
underthebridgeProgression :: [[ChromaticNote]]
underthebridgeProgression = makeTriadProgression fourChordSongProgression $ majorScale $ chromaticNote "E2"
