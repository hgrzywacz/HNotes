{-# LANGUAGE TemplateHaskell #-}

{-|
	Module			: HNotes.Notes
	Description : Convenience package to easily test scales and chords etc.
	Copyright	 : (c) Hubert Grzywacz, 2016
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

module HNotes.Notes where
import Control.Monad
import HNotes
import HNotes.TH

-- * TemplateHaskell generated functions
$(generateAllNotes)
$(generateAllPlacedNotes)

-- * Example notes

-- e1 :: PlacedNote
-- e1 = PlacedNote (E, Base, 1)

-- cs3 :: PlacedNote
-- cs3 = PlacedNote (C, Sharp, 3)

-- a4 :: PlacedNote
-- a4 = PlacedNote (A, Base, 4)

-- c2 :: PlacedNote
-- c2 = PlacedNote (C, Base, 2)

-- ab4 :: PlacedNote
-- ab4 = PlacedNote (A, Flat, 4)

openE :: PlacedNote
openE = e1
