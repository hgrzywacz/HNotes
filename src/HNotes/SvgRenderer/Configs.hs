{-|
	Module			: HNotes.SvgRenderer.Configs
	Copyright	 : (c) Hubert Grzywacz, 2017
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

{-# LANGUAGE OverloadedStrings #-}

module HNotes.SvgRenderer.Configs where

import Control.Monad.Trans.Reader

import Text.Blaze.Svg11

import HNotes
import HNotes.Tunings

import HNotes.SvgRenderer.Translation

data Fill = Black | Grey

data Hand = RightHand | LeftHand

data Naming = English | German -- | EnglishWords | GermanWords

data NoteSpec = NoteSpec {noteS :: ChromaticNote, fillS :: Fill, fretNS :: Integer, stringNS :: Integer}

data FretboardConfig = FretboardConfig {
	tuningC :: Tuning,
	fretsNC :: Integer,
	handnessC :: Hand,
	fretPixelsC :: Integer,
	namingC :: Naming}

stringsNC :: FretboardConfig -> Integer
stringsNC = toInteger . length . tuningC

type SvgConfig = Reader FretboardConfig Svg

rightHandBass :: FretboardConfig
rightHandBass = FretboardConfig {
	tuningC = reverse eStandard,
	fretsNC = 12,
	handnessC = RightHand,
	fretPixelsC = fretPixels',
	namingC = English}

leftHandBass :: FretboardConfig
leftHandBass = FretboardConfig {
	tuningC = reverse eStandard,
	fretsNC = 16,
	handnessC = LeftHand,
	fretPixelsC = fretPixels',
	namingC = German}

leftHandGuitar :: FretboardConfig
leftHandGuitar = FretboardConfig {
	tuningC = reverse eGuitarStandard,
	fretsNC = 16,
	handnessC = LeftHand,
	fretPixelsC = fretPixels',
	namingC = German}
