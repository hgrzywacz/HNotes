{-|
	Module			: HNotes.Pitches
	Copyright	 : (c) Hubert Grzywacz, 2016
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

module HNotes.Pitches where

import HNotes

type KeyNumber = Int

pianoKey :: PlacedNote -> KeyNumber
pianoKey = error "Not yet implemented"

pitch :: Floating a => a -> a
pitch n = (2 ** ((n - 49)/12)) * 440
