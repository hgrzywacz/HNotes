module Main where

import HNotes
import HNotes.Utils
import HNotes.Coloring
import HNotes.Fretboard
import HNotes.Tunings
import HNotes.Scales
import HNotes.Chords
import HNotes.Progressions
import HNotes.Notes
import HNotes.Lilypond
import HNotes.Frequencies
import HNotes.LastingNote
import HNotes.Pitches
import HNotes.TH
import HNotes.LilypondUtils
import HNotes.Diagram
import HNotes.Modes
import HNotes.SvgRenderer

main :: IO ()
main = putStrLn "Main!"
