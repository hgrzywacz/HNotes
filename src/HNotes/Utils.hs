{-|
	Module			: HNotes.Utils
	Description :
	Copyright	 : (c) Hubert Grzywacz, 2017
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

module HNotes.Utils where

(&) = flip ($)

v >$> f = fmap f v
infixl 1 >$>

takeN :: Integer -> [a] -> [a]
takeN = take . fromIntegral

dropN :: Integer -> [a] -> [a]
dropN = drop . fromIntegral
