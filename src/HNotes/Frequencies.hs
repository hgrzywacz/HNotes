module HNotes.Frequencies where
-- module HNotes.Frequencies (frequency) where

import HNotes
import HNotes.Notes

ratio :: Int -> Float
ratio n = 2 ** (fromIntegral n / 12)

frequency :: PlacedNote -> Float
frequency (PlacedNote (A, Base, 4)) = 440
frequency note = ratio (distance note a4) * frequency a4
