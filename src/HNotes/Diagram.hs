module HNotes.Diagram	where

import Control.Monad
import System.Console.ANSI
import Data.List (intersperse)

import HNotes
import HNotes.Scales
import HNotes.Chords

type ColoredNote = (ChromaticNote, IO ())

back2Black = setSGR [SetColor Foreground Dull Black]

colorRun :: [ChromaticNote] -> [ColoredNote]
colorRun notes = zipWith (\ n i -> (n, makeAction n i)) notes [1..]
	where
		makeAction :: ChromaticNote -> Int -> IO ()
		makeAction n i = do
			let color = toEnum $ mod i 7 + 1
			setSGR [SetColor Foreground Dull color]
			if length (show n) == 1
				 then putStr $ show n ++ " "
				 else putStr $ show n

twelveNotes :: Note a => a -> [a]
twelveNotes = ascend 12

printNotesRow :: [ColoredNote] -> IO ()
printNotesRow colored =
	sequence_ $ concat [[putStr "\t|\t"], intersperse (putStr "  ") (map snd colored), [putStr "\n"]]

printRow :: String -> [ChromaticNote] -> [ColoredNote] -> IO ()
printRow label notes allNotes =
	sequence_ $ concat [[back2Black, putStr label], filterdNotes, [putStr "\n"]]
		where
			filterNote note | containsNote notes (fst note) = snd note
											| otherwise = putStr "  "
			filterdNotes = intersperse (putStr "  ") $ map filterNote allNotes

printRun allNotes label run = printRow label run allNotes

printMajorScale :: ChromaticNote -> [ColoredNote] -> IO ()
printMajorScale n = printRow label (majorScale n)
	where
		label = "Major " ++ show n ++ "\t|\t"

startAtEnd :: [a] -> [a]
startAtEnd xs = xs ++ [head xs]

printDiagram :: ChromaticNote -> ChromaticNote -> IO ()
printDiagram startNote n = do
	let colored = startAtEnd $ colorRun $ twelveNotes startNote
	let print = printRun colored

	printNotesRow colored

	let sep = "\t|\t"
	print ("Major " ++ show n ++ sep) $ majorScale n
	print (show n ++ sep) $ majorTriad n
	print (show n ++ "7" ++ sep) $ majorSeventhChord n
	print ("Minor " ++ show n ++ sep) $ minorScale n
	print (show n ++ "m" ++ sep) $ minorTriad n
	print (show n ++ "m7" ++ sep) $ minorSeventhChord n
