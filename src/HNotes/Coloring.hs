{-# LANGUAGE ImpredicativeTypes #-}

module HNotes.Coloring where

import System.Console.ANSI
import Data.List (intersperse)

import HNotes

type Coloring = PlacedNote -> Color

defaultColoring :: Coloring
defaultColoring = toEnum . octave

resetColor :: IO ()
resetColor = setSGR [Reset]

setColor :: Coloring -> PlacedNote -> IO ()
setColor coloring n = do
		let color = coloring n :: Color
		setSGR [SetColor Foreground Dull color]

printBaseNote :: Coloring -> PlacedNote -> IO ()
printBaseNote coloring n = do
		setColor coloring n
		putStr $ show $ key n
		putStr $ show $ octave n
		putStr " "

printAccentedNote :: Coloring -> PlacedNote -> IO ()
printAccentedNote coloring n = do
		setColor coloring n
		putStr $ show $ key n
		putStr $ show $ accent n
		putStr $ show $ octave n

printNoteColor :: Coloring -> PlacedNote -> IO ()
printNoteColor coloring n
	| accent n == Base = printBaseNote coloring n
	| otherwise = printAccentedNote coloring n

printChordColor :: Coloring -> [PlacedNote] -> IO ()
printChordColor coloring ch = do
		putStr "["
		sequence_ $ intersperse (putStr " ") $ map (printNoteColor coloring) ch
		putStr "]"

printPlacedNote :: PlacedNote -> IO ()
printPlacedNote = printNoteColor defaultColoring

printFilteredNote :: (PlacedNote -> Bool) -> PlacedNote -> IO ()
printFilteredNote p n
	| p n = printPlacedNote n
	| otherwise = putStr "  "

printNoteLn :: PlacedNote -> IO ()
printNoteLn n = do
		printPlacedNote n
		putStrLn ""

printChord :: [PlacedNote] -> IO ()
printChord = printChordColor defaultColoring

printChordLn :: [PlacedNote] -> IO ()
printChordLn ch = do
	printChord ch
	putStrLn ""

printScale :: [PlacedNote] -> IO ()
printScale = printChord

printScaleLn :: [PlacedNote] -> IO ()
printScaleLn = printChordLn
