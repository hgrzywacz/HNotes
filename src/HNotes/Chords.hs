{-|
	Module			: HNotes.Chords
	Description : Collection of function and constants to generate chords.
	Copyright	 : (c) Hubert Grzywacz, 2016
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

module HNotes.Chords (
	majorTriad,
	minorTriad,
	diminishedTriad,
	majorSeventhChord,
	minorSeventhChord,
	dominantSeventh,
	suspended2,
	suspended4,
	suspended
) where

import HNotes
import HNotes.Scales

-- * Degrees
-- | Degrees functions take an array of notes as an argument and return one
-- note.

second :: [a] -> a
second scale' = scale' !! 1

third :: [a] -> a
third scale' = scale' !! 2

fourth :: [a] -> a
fourth scale' = scale' !! 3

fifth :: [a] -> a
fifth scale' = scale' !! 4

sixth :: [a] -> a
sixth scale' = scale' !! 5

seventh :: [a] -> a
seventh scale' = scale' !! 6

-- * Intervals

majorSecond :: (Note a) => a -> a
majorSecond = second . majorScale

minorSecond :: (Note a) => a -> a
minorSecond = second . minorScale

majorThird :: (Note a) => a -> a
majorThird = third . majorScale

minorThird :: (Note a) => a -> a
minorThird = third . minorScale

perfectFourth :: (Note a) => a -> a
perfectFourth = fourth . majorScale

perfectFifth :: (Note a) => a -> a
perfectFifth = fifth . majorScale

majorSixth :: (Note a) => a -> a
majorSixth = sixth . majorScale

majorSeventh :: (Note a) => a -> a
majorSeventh = seventh . majorScale

-- | From Wikipedia:
-- an <https://en.wikipedia.org/wiki/Augmented_fifth augmented fifth>
-- is an interval produced by widening a perfect fifth by a chromatic
-- semitone.
augumentedFifth :: (Note a) => a -> a
augumentedFifth = halfup . perfectFifth

diminishedFifth :: (Note a) => a -> a
diminishedFifth = halfdown . perfectFifth

minorSeventh :: (Note a) => a -> a
minorSeventh = seventh . minorScale

-- * Chords

-- ** Triads formulas

majorTriadIntervals :: (Note a) => [a -> a]
majorTriadIntervals = [root, majorThird, perfectFifth]

majorTriad :: (Note a) => a -> [a]
majorTriad note' = chord note' majorTriadIntervals

minorTriadIntervals :: (Note a) => [a -> a]
minorTriadIntervals = [root, minorThird, perfectFifth]

minorTriad :: (Note a) => a -> [a]
minorTriad note' = chord note' minorTriadIntervals

augumentedTriadIntervals :: (Note a) => [a -> a]
augumentedTriadIntervals = [root, majorThird, augumentedFifth]

augumentedTriad :: (Note a) => a -> [a]
augumentedTriad note' = chord note' augumentedTriadIntervals

diminishedTriadIntervals :: (Note a) => [a -> a]
diminishedTriadIntervals = [root, minorThird, diminishedFifth]

diminishedTriad :: (Note a) => a -> [a]
diminishedTriad note' = chord note' diminishedTriadIntervals

diminished :: (Note a) => a -> [a]
diminished = map toSharp . diminishedTriad

-- ** Sixth chords

majorSixthIntervals :: (Note a) => [a -> a]
majorSixthIntervals = majorTriadIntervals ++ [majorSixth]

majorSixthChord :: (Note a) => a -> [a]
majorSixthChord note' = chord note' majorSixthIntervals

-- ** Seventh chords

majorSeventhIntervals :: (Note a) => [a -> a]
majorSeventhIntervals = majorTriadIntervals ++ [majorSeventh]

majorSeventhChord :: (Note a) => a -> [a]
majorSeventhChord note' = chord note' majorSeventhIntervals

minorSeventhIntervals :: (Note a) => [a -> a]
minorSeventhIntervals = minorTriadIntervals ++ [minorSeventh]

minorSeventhChord :: (Note a) => a -> [a]
minorSeventhChord note' = chord note' minorSeventhIntervals

dominantSeventhIntervals :: (Note a) => [a -> a]
dominantSeventhIntervals = majorTriadIntervals ++ [minorSeventh]

dominantSeventh :: (Note a) => a -> [a]
dominantSeventh note' = chord note' dominantSeventhIntervals

-- ** Suspended chords

suspended2Intervals :: (Note a) => [a -> a]
suspended2Intervals = [root, majorSecond, perfectFifth]

suspended2 :: (Note a) => a -> [a]
suspended2 note' = chord note' suspended2Intervals

suspended4Intervals :: (Note a) => [a -> a]
suspended4Intervals = [root, perfectFourth, perfectFifth]

suspended4 :: (Note a) => a -> [a]
suspended4 note' = chord note' suspended4Intervals

suspended :: (Note a) => a -> [a]
suspended = suspended4
