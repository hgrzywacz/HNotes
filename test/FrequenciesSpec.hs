module FrequenciesSpec where

import Test.Hspec
import Test.Hspec.QuickCheck

import Test.QuickCheck
import Test.QuickCheck.Gen

import HNotes
import HNotes.Notes
import HNotes.Frequencies

freq :: PlacedNote -> Float
freq = HNotes.Frequencies.frequency

compare2places :: Float -> Float -> Bool
compare2places a b = (round (a * 100)) == (round (b * 100))

spec_a4 :: Expectation
spec_a4 = (freq a4) `shouldSatisfy` (compare2places 440.0)

setExpectation :: PlacedNote -> Float -> Expectation
setExpectation pn fr = (freq pn) `shouldSatisfy` (compare2places fr)

setDistanceExpectation :: PlacedNote -> Int -> Expectation
setDistanceExpectation pn1 dist = (distance a4 pn1) `shouldBe` dist

spec :: Spec
spec = do
  it "a4" $ spec_a4

  it "gsharp4" $ setDistanceExpectation gsharp4 1
  it "a4" $ setDistanceExpectation a4 0
  it "asharp4" $ setDistanceExpectation asharp4 (-1)
  it "b4" $ setDistanceExpectation b4 (-2)
  it "a5" $ setDistanceExpectation a5 (-12)
  it "a3" $ setDistanceExpectation a3 12

  it "c0" $ setExpectation c0 16.35
  it "a0" $ setExpectation a0 27.50
  it "asharp0" $ setExpectation asharp0 29.14
  it "d1" $ setExpectation d1 36.71
  it "c2" $ setExpectation c2 65.41
  it "csharp2" $ setExpectation csharp2 69.30
  it "d2" $ setExpectation d2 73.42
  it "dsharp2" $ setExpectation dsharp2 77.78
  it "b2" $ setExpectation b2 123.47
  it "f3" $ setExpectation f3 174.61
  it "fsharp3" $ setExpectation fsharp3 185.00
  it "g3" $ setExpectation g3 196.00
  it "gsharp3" $ setExpectation gsharp3 207.65
  it "a3" $ setExpectation a3 220.00
  it "e4" $ setExpectation e4 329.63
  it "b4" $ setExpectation b4 493.88
  it "csharp5" $ setExpectation csharp5 554.37
  it "d5" $ setExpectation d5 587.33
  it "f6" $ setExpectation f6 1396.91
  it "csharp7" $ setExpectation csharp7 2217.46
  it "d7" $ setExpectation d7 2349.32
  it "dsharp7" $ setExpectation dsharp7 2489.02
  it "e8" $ setExpectation e8 5274.04
  it "f8" $ setExpectation f8 5587.65
  it "fsharp8" $ setExpectation fsharp8 5919.91
  it "g8" $ setExpectation g8 6271.93
