module HNotesSpec where

import Test.Hspec
import Test.Hspec.QuickCheck

import Test.QuickCheck
import Test.QuickCheck.Gen

import Data.List (intersect)

import HNotes
import HNotes.Scales

-- * Simple tests

-- | A#2 -> B2
test_halfupSharp :: Expectation
test_halfupSharp = (halfup (PlacedNote (A, Sharp, 2))) `shouldBe` (PlacedNote (B, Base, 2))

-- | Bb2 -> B2
test_halfupFlat :: Expectation
test_halfupFlat = (halfup (PlacedNote (B, Flat, 2))) `shouldBe` (PlacedNote (B, Base, 2))

-- | A#2 -> A2
test_halfdownSharp :: Expectation
test_halfdownSharp = (halfdown (PlacedNote (A, Sharp, 2))) `shouldBe` (PlacedNote (A, Base, 2))

-- | Bb2 -> A2
test_halfdownFlat :: Expectation
test_halfdownFlat = (halfdown (PlacedNote (B, Flat, 2))) `shouldBe` (PlacedNote (A, Base, 2))

spec_steps_chromatic :: SpecWith ()
spec_steps_chromatic = do
  describe "halfup ChromaticNote" $ do
    it "A# -> B" $ (halfup (ChromaticNote (A, Sharp))) `shouldBe` (ChromaticNote (B, Base))
    it "Bb -> B" $ (halfup (ChromaticNote (B, Flat))) `shouldBe` (ChromaticNote (B, Base))
  describe "halfdown ChromaticNote" $ do
    it "A# -> A" $ (halfdown (ChromaticNote (A, Sharp))) `shouldBe` (ChromaticNote (A, Base))
    it "Bb -> A" $ (halfdown (ChromaticNote (B, Flat))) `shouldBe` (ChromaticNote (A, Base))

spec_steps_placed:: SpecWith ()
spec_steps_placed = do
  describe "halfup PlacedNote" $ do
    it "A#2 -> B2" $ test_halfupSharp
    it "Bb2 -> B2" $ test_halfupFlat
  describe "halfdown PlacedNote" $ do
    it "A#2 -> A2" $ test_halfdownSharp
    it "Bb2 -> A2" $ test_halfdownFlat

placedA4 = PlacedNote (A, Base, 4)

spec_octaveUp :: SpecWith ()
spec_octaveUp = do
  describe "Octave up" $ do
    it "A4 -> A5" $ (octaveUp placedA4) `shouldBe` (PlacedNote (A, Base, 5))

spec_fromEnum_placed :: SpecWith ()
spec_fromEnum_placed = do
  describe "fromEnum - PlacedNote" $ do
    it "A4 -> 49" $ (fromEnum (PlacedNote (A, Base, 4))) `shouldBe` (57 :: Int)

spec_Eq :: SpecWith ()
spec_Eq = do
  describe "Eq" $ do
    it "A4 == A4" $ (placedA4 == placedA4) `shouldBe` True
    it "B#3 == Cb3" $ ((PlacedNote (B, Sharp, 3)) == (PlacedNote (C, Flat, 3))) `shouldBe` True
    it "A4 /= C4" $ (placedA4 /= (PlacedNote (C, Base, 4))) `shouldBe` True
    it "~ A#4 /= Bb4" $ (PlacedNote (A, Sharp, 4) /= (PlacedNote (B, Flat, 4))) `shouldBe` False

spec_Ord :: SpecWith ()
spec_Ord = do
  describe "Ord" $ do
    it "A4 < A5" $ (PlacedNote (A, Base, 4) < PlacedNote (A, Base, 5)) `shouldBe` True
    it "D4 < G4" $ (PlacedNote (D, Base, 4) < PlacedNote (G, Base, 4)) `shouldBe` True
    it "B4 <= B4" $ (PlacedNote (B, Base, 4) <= PlacedNote (B, Base, 4)) `shouldBe` True
    it "A4 < A#4" $ (PlacedNote (A, Base, 4) < PlacedNote (A, Sharp, 4)) `shouldBe` True
    it "A#4 > A4" $ (PlacedNote (A, Sharp, 4) > PlacedNote (A, Base, 4)) `shouldBe` True
    it "A4 >= A#4" $ (PlacedNote (A, Base, 4) < PlacedNote (A, Sharp, 4)) `shouldBe` True
    it "max A4 C5 == C5" $ (max placedA4 (PlacedNote (C, Base, 5))) `shouldBe` (PlacedNote (C, Base, 5))

instance Arbitrary Key where
  arbitrary = elements [C, D, E, F, G, A, B]

instance Arbitrary Accent where
  arbitrary = elements [Sharp, Base, Flat]

arbitraryOctave :: Gen Octave
arbitraryOctave = elements $ [0..9]

instance Arbitrary ChromaticNote where
  arbitrary = do
    k <- arbitrary

    -- omit B♯, E♯, C♭, F♭
    let af k | elem k [B, E] = elements [Base, Flat]
             | elem k [C, F] = elements [Base, Sharp]
             | otherwise = arbitrary

    a <- af k

    return $ ChromaticNote (k, a)

instance Arbitrary PlacedNote where
  arbitrary = do
    o <- arbitraryOctave
    chrom_n <- arbitrary

    return $ chromaticToPlaced o chrom_n

prop_twoupChangesKey :: PlacedNote -> Bool
prop_twoupChangesKey note = (key note) /= (key $ oneup $ oneup note)

spec_twoupChangesKey :: Spec
spec_twoupChangesKey = prop "two up changes key" $ prop_twoupChangesKey

prop_scalesAreHeptatonic :: PlacedNote -> Bool
prop_scalesAreHeptatonic n = (length $ majorScale n) == 8 && (length $ minorScale n) == 8

spec_scalesAreHeptatonic :: Spec
spec_scalesAreHeptatonic = prop "scales are heptatonic" $ prop_scalesAreHeptatonic

prop_parralelDiatonicsShare5Notes :: PlacedNote -> Bool
prop_parralelDiatonicsShare5Notes n =
  (length $ intersect (minorScale n) (majorScale n)) == 5

prop_halfupsOfAccentsAreEqual :: PlacedNote -> Bool
prop_halfupsOfAccentsAreEqual n = (halfup n) == (halfup $ swapAccents n)

spec_halfupsOfAccentsAreEqual :: Spec
spec_halfupsOfAccentsAreEqual =
  prop "halfups of equal notes should be the same" $ prop_halfupsOfAccentsAreEqual

prop_scalesFromAccentsAreEqual :: PlacedNote -> Bool
prop_scalesFromAccentsAreEqual n = (majorScale n) == (majorScale $ swapAccents n)

spec_scalesFromAccentsAreEqual :: Spec
spec_scalesFromAccentsAreEqual =
  prop "scales made from equivalent sharp and flat are equal" $ prop_scalesFromAccentsAreEqual

spec_parralelDiatonicsShare5Notes :: Spec
spec_parralelDiatonicsShare5Notes =
  prop "parralel diatonic scales share 5 notes" $ prop_parralelDiatonicsShare5Notes

spec_fromEnumIsToEnum :: Spec
spec_fromEnumIsToEnum = do
  prop "chromaticNotes fromEnum == toEnum" $ prop_chromatic
  prop "placedNotes fromEnum == toEnum" $ prop_placed
    where
      prop_chromatic :: ChromaticNote -> Bool
      prop_chromatic note = note == (toEnum (fromEnum note))
      prop_placed :: PlacedNote -> Bool
      prop_placed note = note == (toEnum (fromEnum note))

spec_props :: SpecWith ()
spec_props = do
  describe "properties" $ do
    spec_twoupChangesKey
    spec_scalesAreHeptatonic
    spec_halfupsOfAccentsAreEqual
    spec_scalesFromAccentsAreEqual
    spec_parralelDiatonicsShare5Notes

spec_conversion :: SpecWith ()
spec_conversion = do
  describe "placedToChromatic, chromaticToPlaced" $ do
    it "A#3 -> A#" $ (placedToChromatic (PlacedNote (A, Sharp, 3))) `shouldBe` (ChromaticNote (A, Sharp))
    it "3 A# -> A#3" $ (chromaticToPlaced 3 (ChromaticNote (A, Sharp))) `shouldBe` (PlacedNote (A, Sharp, 3))

spec_toChromatic :: SpecWith ()
spec_toChromatic = do
  describe "toChromatic" $ do
    it "A#3 -> A#" $ (toChromatic (PlacedNote (A, Sharp, 3))) `shouldBe` (ChromaticNote (A, Sharp))
    it "Ab -> Ab" $ (toChromatic (ChromaticNote (A, Flat))) `shouldBe` (ChromaticNote (A, Flat))

spec_readChromaticNote :: SpecWith ()
spec_readChromaticNote = do
  describe "chromaticNote from string" $ do
    it "C" $ (chromaticNote "C") `shouldBe` (ChromaticNote (C, Base))
    it "A#" $ (chromaticNote "A#") `shouldBe` (ChromaticNote (A, Sharp))
    it "Gb" $ (chromaticNote "Gb") `shouldBe` (ChromaticNote (G, Flat))

spec_scales :: SpecWith ()
spec_scales = do
  describe "scales" $ do
    it "C major - chromatic" $
      (majorScale (ChromaticNote (C, Base))) `shouldBe`
      (map chromaticNote ["C", "D", "E", "F", "G", "A", "B", "C"])
    it "C major - placed" $
      (majorScale (PlacedNote (C, Base, 2))) `shouldBe`
      (map placedNote ["C2", "D2", "E2", "F2", "G2", "A2", "B2", "C3"])

spec :: Spec
spec = do
  spec_fromEnumIsToEnum
  spec_steps_placed
  spec_octaveUp
  spec_fromEnum_placed
  spec_steps_chromatic
  spec_conversion
  spec_toChromatic
  spec_readChromaticNote
  spec_scales
  spec_props
  spec_Eq
  spec_Ord

main :: IO ()
main = hspec spec
