module ProgressionsSpec where

import Test.Hspec
import Test.Hspec.QuickCheck

import Test.QuickCheck
import Test.QuickCheck.Gen

import HNotes
import HNotes.Scales
import HNotes.Progressions

-- >>> triadByNumber "ii" $ c2
-- [D2,F2,A2]
spec_iiChordOfC :: Expectation
spec_iiChordOfC = (triadByNumber "ii" $ majorScale $ chromaticNote "C") `shouldBe` correctTriad
  where
    -- [D2,F2,A2]
    correctTriad = map chromaticNote ["D", "F", "A"]


spec :: Spec
spec = do
  it "ii triad chord of C scale" $ spec_iiChordOfC
