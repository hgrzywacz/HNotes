module HNotes.Modes where

import Safe (atMay)

import HNotes
import HNotes.Scales

data Degree =
		Tonic
	| Supertonic
	| Mediant
	| Subdominant
	| Dominant
	| Submediant
	| Subtonic deriving (Show, Enum)

getDegree :: Note a => Degree -> [a] -> Maybe a
getDegree d scale = atMay scale (fromEnum d)

data Mode =
		Ionian
	| Dorian
	| Phrygian
	| Lydian
	| Mixolydian
	| Aeolian
	| Locrian deriving (Show, Enum)

makeScaleSteps :: Note a => Mode -> [a -> a]
makeScaleSteps m = take 7 $ drop (fromEnum m) $ cycle majorScaleSteps

makeScale :: Note a => a -> Mode -> [a]
makeScale n m = scale n $ makeScaleSteps m

makeAllModes :: Note a => a -> [(Mode, [a])]
makeAllModes n = zip modes (map (makeScale n) modes)
	where
		modes = map toEnum [0..6]

padL :: Int -> String -> String
padL n s
		| length s < n	= s ++ replicate (n - length s) ' '
		| otherwise		 = s

showModes :: Note a => [(Mode, [a])] -> IO ()
showModes = mapM_ (putStrLn . (\m -> padL 11 (show $ fst m) ++ show (snd m)))
