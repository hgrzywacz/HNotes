{-|
	Module			: HNotes.SvgRenderer
	Copyright	 : (c) Hubert Grzywacz, 2017
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

{-# LANGUAGE OverloadedStrings #-}

-- module HNotes.SvgRenderer.Background (background) where
module HNotes.SvgRenderer.Background where

import Control.Monad.Trans.Reader

import Data.Text (pack)

import Text.Blaze.Svg11 (Svg, AttributeValue, rect, (!), translate, g, textValue, ellipse)
import Text.Blaze.Internal (MarkupM)
import qualified Text.Blaze.Svg11.Attributes as A

import HNotes.Utils

import HNotes.SvgRenderer.Configs
import HNotes.SvgRenderer.Translation

type Range = (Integer, Integer)

packAttribute :: Show a => a -> AttributeValue
packAttribute = textValue . pack . show

enforceSvg :: MarkupM () -> Svg
enforceSvg = id

bridge :: Range -> Reader FretboardConfig Svg
bridge range = do
	config <- ask

	let starting = fst range

	let fretPixels = fretPixelsC config

	let fretsN = fretsNC config

	let handness = handnessC config

	let translation = case handness of
				RightHand -> 0
				LeftHand -> fretPixels * (fretsN - 1)

	if starting == 0
	 then
		 return $ (rect !
				A.width "6" ! A.height (packAttribute (2 * originY + 4 * stringPixels)) !
				A.fill "#000000" !
				A.transform (translate (translation + (fretPixels - 3)) 0))
	 else
		 return mempty

fret :: Integer -> Integer -> Svg
fret fretPixels n = do
	rect !
		A.width "3" ! A.height (packAttribute (2 * originY + 4 * stringPixels)) !
		A.fill "#000000" !
		A.transform (translate (fretPixels * n) 0)

frets :: Range -> Reader FretboardConfig Svg
frets range = do
	config <- ask
	let fretPixels = fretPixelsC config

	let start = if ((fst range) == 0) then 2 else 1
	let fretsN = snd range

	let fretsRange = case (handnessC config) of
				RightHand -> [start..(start + fretsN)]
				LeftHand -> [1..(fretsN - 1)]

	return $ sequence_ $ map (fret fretPixels) fretsRange

stringLine :: Integer -> Integer -> Integer -> Svg
stringLine fretPixels nFrets stringN = do
	rect !
		A.width (packAttribute (nFrets * fretPixels)) !
		A.height "4" !
		A.fill "#000000" !
		A.transform (translate 0 (originY + stringPixels * stringN - 2))

stringLines :: Range -> Reader FretboardConfig Svg
stringLines range = do

	config <- ask

	let nFrets = snd range
	let nStrings = stringsNC config
	let fretPixels = fretPixelsC config

	return $ sequence_ $ map (stringLine fretPixels (nFrets + 1)) [1..nStrings]

singleDot :: Integer -> Svg
singleDot fretN =
	g ! A.transform (makeTranslation fretN 0) $ do
		ellipse ! A.rx "10" ! A.ry "10" ! A.cx "0" ! A.cy "0" ! A.fill "#000000"

doubleDot :: Integer -> Svg
doubleDot fretN =
	g ! A.transform (makeTranslation fretN 0) $ do
		ellipse ! A.rx "10" ! A.ry "10" ! A.cx "13" ! A.cy "0" ! A.fill "#000000"
		ellipse ! A.rx "10" ! A.ry "10" ! A.cx "-13" ! A.cy "0" ! A.fill "#000000"

dots :: Range -> Reader FretboardConfig Svg
dots range = do
	config <- ask

	let start = fst range

	let fretsN = snd range

	-- let r = [1..(fretsN +1)]
	let r = [start..(start + fretsN)]
	let absR = [2..fretsN]

	let dotsRange = case (handnessC config) of
				RightHand -> zip absR r
				LeftHand	-> zip [0..fretsN] (reverse [1..fretsN])

	return $ sequence_ $ map mapToDots dotsRange
		where
			mapToDots :: (Integer, Integer) -> Svg
			mapToDots (n, nn)
				| p 3 = singleDot n
				| p 5 = singleDot n
				| p 7 = singleDot n
				| p 9 = singleDot n
				| p 0 = doubleDot n
				| otherwise = mempty
				where
					p :: Integer -> Bool
					p i = mod nn 12 == i

rangeBackground :: Range -> Reader FretboardConfig Svg
rangeBackground range = do

	bridgeSvg <- bridge range
	stringLinesSvg <- stringLines range
	fretsSvg <- frets range
	dotsSvg <- dots range

	return $ do
		bridgeSvg
		stringLinesSvg
		fretsSvg
		dotsSvg

background :: Reader FretboardConfig Svg
background = do
	config <- ask

	let fretsN = fretsNC config
	rangeBackground (0, fretsN)
