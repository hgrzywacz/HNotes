module ChordsSpec where

import Test.Hspec
import Test.Hspec.QuickCheck

import Test.QuickCheck
import Test.QuickCheck.Gen

import HNotes
import HNotes.Notes
import HNotes.Scales
import HNotes.Progressions
import HNotes.Chords

-- >>> suspended2 c
-- [C, D, G]
spec_Csus2 :: Expectation
spec_Csus2 = (suspended2 c) `shouldBe` (map chromaticNote ["C", "D", "G"])

-- >>> suspended c
-- [C, F, G]
spec_Csus :: Expectation
spec_Csus = (suspended c) `shouldBe` (map chromaticNote ["C", "F", "G"])

-- >>> diminished c
-- [C, D#, F#]
spec_Cdim :: Expectation
spec_Cdim = (diminishedTriad c) `shouldBe` ([c, dsharp, fsharp])

spec :: Spec
spec = do
  it "Csus2" $ spec_Csus2
  it "Csus" $ spec_Csus
  it "Cdim" $ spec_Cdim
