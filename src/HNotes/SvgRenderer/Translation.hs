{-|
	Module			: HNotes.SvgRenderer.Translation
	Copyright	 : (c) Hubert Grzywacz, 2017
	License		 :
	Maintainer	: hgrzywacz@gmail.com
	Stability	 : experimental
-}

{-# LANGUAGE OverloadedStrings #-}

module HNotes.SvgRenderer.Translation where

import Text.Blaze.Svg11

originX :: Integer
originX = 50

originY :: Integer
originY = 30

fretPixels' :: Integer
fretPixels' = 100

stringPixels :: Integer
stringPixels = 50

makeTranslation :: Integer -> Integer -> AttributeValue
makeTranslation fretN stringN = translate (originX + fretPixels' * fretN) (originY + stringPixels * stringN)
