module HNotes.LilypondUtils where

import HNotes
import HNotes.Lilypond
import HNotes.Notes
import HNotes.Scales
import HNotes.Chords

type ChromaticDegree = ChromaticScale -> ChromaticNote

shiftBy :: Int -> [a] -> [a]
shiftBy n xs = drop n xs ++ take n xs

-- lilypondTripletsRun :: Int -> ChromaticScale -> Int -> IO ()
-- lilypondTripletsRun n scale degree = do
--	 let shiftedScale = take 12 $ cycle $ shiftBy degree $ take 7 scale
--	 let reversedScale = shiftBy 1 $ reverse $ shiftedScale
--	 let cycled = take (3 * n) $ cycle $ shiftedScale ++ reversedScale
--	 let groupped = groupBy3 cycled []

--	 let printed = map (\t -> putStrLn $ "\\tuplet 3/2 {" ++ (lilypond t) ++ "}") groupped

--	 sequence_ printed

--		 where
--			 groupBy3 :: [a] -> [[a]] -> [[a]]
--			 groupBy3 [] memo = memo
--			 groupBy3 xs memo = groupBy3 (drop 3 xs) (memo ++ [(take 3 xs)])

-- testRun = lilypondTripletsRun 15 (majorScale c) 2
